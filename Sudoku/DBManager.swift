//
//  DBManager.swift
//  Sudoku
//
//  Created by Camil Milos on 06/01/2020.
//  Copyright © 2020 Camil Milos. All rights reserved.
//

import Foundation
import RealmSwift

class DBManager {
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        
        database = try! Realm()
        
    }
    
    func getDataFromDB() -> Results<StorageEntity> {
        
        let results: Results<StorageEntity> = database.objects(StorageEntity.self)
        return results
    }
    
    func addData(object: StorageEntity) {
        
        try! database.write {
            database.add(object, update: .all)
            print("Added new object")
        }
    }
    
    func deleteAllDatabase()  {
        try! database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(object: StorageEntity) {
        
        try! database.write {
            
            database.delete(object)
        }
    }
    
}
