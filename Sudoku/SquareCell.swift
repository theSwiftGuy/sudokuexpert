//
//  SquareCell.swift
//  Sudoku
//
//  Created by Camil Milos on 23/11/2019.
//  Copyright © 2019 Camil Milos. All rights reserved.
//

import UIKit

class SquareCell: UIView {
    
    let defaultColor: UIColor = .white
    let defaultTextColor: UIColor = .black
    
    fileprivate lazy var label: UILabel! = {
        let lbl = UILabel()
        lbl.textColor = labelTextColor
        lbl.font = .systemFont(ofSize: 32, weight: .light)
        lbl.textAlignment = .center
        lbl.backgroundColor = defaultColor
        lbl.layer.borderColor = UIColor.lightGray.cgColor;
        lbl.layer.borderWidth = 0.5
        
        #if DEBUG
            lbl.text = "0"
        #endif
        
        return lbl
    }()
    
    var number: Int = 0 {
        didSet {
            label.text = "\(number)"
        }
    }
    
    var enteredNumber: Int = 0 {
        didSet {
            
            if enteredNumber != 0 {
            
                if self.number != enteredNumber {
                    //self.label.textColor = .red
                    labelTextColor = .red
                    label.text = "\(enteredNumber)"
                } else {
                    //self.label.textColor = .blue
                    labelTextColor = .blue
                    label.text = "\(number)"
                }
                
                self.label.textColor = labelTextColor
            }
        }
    }
    
    var index: Int = 0
    var groupId: Int = 0
    var isEditable: Bool = false
    var labelTextColor: UIColor = .black
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        build()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func build() {
        self.addSubview(label)
                
        label.mas_makeConstraints { (make) in
            
            make?.edges.equalTo()(self)
        }
    }
    
    func changeColor(newColor: UIColor) {
        
        if label.backgroundColor == newColor {
//            label.backgroundColor = .white
        } else {
            label.backgroundColor = newColor
        }
    }
    
    func resetColor() {
        label.backgroundColor = defaultColor
        //label.textColor = .black
    }
    
    func hideLabelNumber() {
        label.text = ""
    }
    
    func eraseLabelNumber() {
        if isEditable {
            enteredNumber = 0
            hideLabelNumber()
        }
    }
    
    func isLocked() -> Bool {
        return (label.text != "")
    }
    
    func hasSquareNumber(number: Int) -> Bool {
        return (self.number == number)
    }
    
    func corectTextLabel() -> Bool {
        return label.textColor != .red
    }
    
        
}
