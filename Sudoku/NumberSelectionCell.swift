//
//  NumberSelectionCell.swift
//  Sudoku
//
//  Created by Camil Milos on 02/12/2019.
//  Copyright © 2019 Camil Milos. All rights reserved.
//

import UIKit

protocol NumberSelectionDelegate: NSObject {
    func didSelect(number: Int)
}

class NumberSelectionCell: UIView {

    let defaultColor: UIColor = .white

    fileprivate lazy var label: UILabel! = {
        let lbl = UILabel()
        lbl.textColor = .black //UIColor(red: 14/255, green: 0/255, blue: 219/255, alpha: 1.0)
        lbl.font = .systemFont(ofSize: 32, weight: .light)
        lbl.textAlignment = .center
        lbl.backgroundColor = defaultColor
//        lbl.layer.borderColor = UIColor.lightGray.cgColor;
//        lbl.layer.borderWidth = 0.5
        
        #if DEBUG
            lbl.text = "0"
        #endif
        
        return lbl
    }()

    var selectedNumber: Int = 0 {
        didSet {
            label.text = "\(selectedNumber)"
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        build()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func build() {
        self.addSubview(label)
                
        label.mas_makeConstraints { (make) in
            
            make?.edges.equalTo()(self)
        }
    }
}

class NumberSelectionView: UIView {
    
    
    weak var delegate: NumberSelectionDelegate?
    
    fileprivate lazy var row: UIView = {
       
        let view = UIView(frame: .zero)
        return view
    }()
    
    fileprivate var numbersCell: [NumberSelectionCell] {
        
        return row.subviews as! [NumberSelectionCell]
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        build()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func build() {
        
        let view = row
        var keyCell: NumberSelectionCell?
        var prevCell: NumberSelectionCell!
        
        for y in 0..<9 {
            
            let numberCell = NumberSelectionCell(frame: .zero)
            numberCell.selectedNumber = y + 1
            
            view.addSubview(numberCell)
            
            if let ks = keyCell {
                
                if y == 8 {
                        
                        numberCell.mas_makeConstraints { (make) in

                           make?.size.equalTo()(ks)
                            make?.centerY.equalTo()(prevCell.mas_centerY)
                            make?.left.equalTo()(prevCell.mas_right)
                           make?.trailing.equalTo()(view.mas_trailing)
                           make?.bottom.equalTo()(view.mas_bottom)

                       }

                    }
                    else {
                    
                        numberCell.mas_makeConstraints { (make) in
                            make?.size.equalTo()(ks)
                            make?.centerY.equalTo()(prevCell.mas_centerY)
                            make?.left.equalTo()(prevCell.mas_right)
                        }
                        prevCell = numberCell
                    }
            }
            else {
                
                numberCell.mas_makeConstraints { (make) in
                    make?.top.equalTo()(view.mas_top)
                    make?.leading.equalTo()(view.mas_leading)
                }
                
                keyCell = numberCell
                prevCell = keyCell
            }

            
        }
        
        self.addSubview(view)
        
        view.mas_makeConstraints { (make) in

            make?.edges.equalTo()(self)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: self)
            let calcIndex = (Int(Int(position.x) / 40) + Int(Int(position.y) / 40)) % 9
            
            if numbersCell[calcIndex].label.text != "" {
                self.delegate?.didSelect(number: numbersCell[calcIndex].selectedNumber)
            }
            
        }
    }
    
    func hideNumberSelection(with index: Int) {
        numbersCell[index-1].label.text = ""
    }

    func unhideNumberSelection(with index: Int) {
        numbersCell[index-1].label.text = "\(index)"
    }
    
    func resetNumbers() {
        numbersCell.forEach {
            $0.label.text = "\($0.selectedNumber)"
        }
    }
}
