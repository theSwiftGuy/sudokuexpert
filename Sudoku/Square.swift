//
//  Square.swift
//  Sudoku
//
//  Created by Camil Milos on 23/11/2019.
//  Copyright © 2019 Camil Milos. All rights reserved.
//

import UIKit

protocol SquareDelegate: NSObject {
    func presentAlert()
    func presentGameFinishedNotification()
    func updateMistakeCounter(value: Int)
    func hideNumberSelection(with index: Int)
    func unhideNumberSelection(with index: Int)
    
}

//protocol SquareNumberSelectionDelegate: NSObject {
//    func hideNumberSelection(with index: Int)
//}

class Square: UIView {
    
    weak var delegate: SquareDelegate?
//    weak var delegateNumberSelection: SquareNumberSelectionDelegate?
    
    fileprivate lazy var square: UIView = {
       
        let view = UIView(frame: .zero)
        return view
    }()
    
    fileprivate var squares: [SquareCell] {
        
        return square.subviews as! [SquareCell]
    }
    
    fileprivate var numSquares: Int {
        return 9
    }
    
    fileprivate var indexXY: (Int, Int) = (0, 0)
    
    var selectedSquare: SquareCell?
    
    var mistakesCounter: Int  = 0
    
    var solutionNumbers: [Int] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    var enteredNumbers: [Int] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        build()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func build() {
    
        let view = square
        
        let XY = Int(Double(numSquares).squareRoot())
        
        var keySquare: SquareCell?
        var prevSquare: SquareCell!
        var endOfRow = false
        
        var groupIndex = 0
        var tempGroupIndex = 0
        
        for y in 0..<XY {
            
            groupIndex = ( y / 3 ) * 3;
            
            for x in 0..<XY {
                
                
                let cell = SquareCell(frame: .zero)
                cell.index = y * XY + x
                
                cell.groupId = groupIndex
                tempGroupIndex = tempGroupIndex + 1
                if (tempGroupIndex % 3) == 0 {
                    groupIndex = groupIndex + 1
                    tempGroupIndex = 0
                }
                view.addSubview(cell)
                
                if let ks = keySquare {
                
                    if endOfRow {
                        
                        cell.mas_makeConstraints { (make) in
                                                   
                           make?.size.equalTo()(ks)
                           make?.left.equalTo()(ks.mas_left)
                           make?.top.equalTo()(ks.mas_bottom)
                       
                       }
                        
                        endOfRow = false
                
                        keySquare  = cell
                        prevSquare = cell
                    }
                    else {
                        
                        cell.mas_makeConstraints { (make) in
                            
                            make?.size.equalTo()(ks)
                            make?.centerY.equalTo()(prevSquare.mas_centerY)
                            make?.left.equalTo()(prevSquare.mas_right)
                        
                            if x == XY - 1 {
                                
                                make?.trailing.equalTo()(view.mas_trailing)
                                
                                if y == XY - 1 {
                                    
                                    make?.bottom.equalTo()(view.mas_bottom)
                                }
                                else {
                                    
                                    endOfRow = true
                                }
                            }
                        }
                        
                        prevSquare = cell
                    }
                    
                }
                else {
                    
                    cell.mas_makeConstraints { (make) in
                        make?.top.equalTo()(view.mas_top)
                        make?.leading.equalTo()(view.mas_leading)
                    }
                    
                    keySquare = cell
                    prevSquare = keySquare
                }
            }
        }

        self.addSubview(view)
        
        view.mas_makeConstraints { (make) in
            
            make?.edges.equalTo()(self)
        }
        
        buildVerticalLine(squares[3])
        buildVerticalLine(squares[6])
        buildHorizontalLine(squares[27])
        buildHorizontalLine(squares[54])
        
        
        selectedSquare = squares[0]
//        let myColour = UIColor(red: 168/255, green: 192/255, blue: 219/255, alpha: 1.0)
//        selectedSquare!.changeColor(newColor: myColour)
        initialHighlightedSquares()
        //selectedSquare = squares[0]
    }
    
    func buildVerticalLine(_ squareCell: SquareCell) {
        let line = UIView(frame: .zero)
        line.backgroundColor = .black
        addSubview(line)
        
        line.mas_makeConstraints{ (make) in
            make?.top.equalTo()(square.mas_top)
            make?.bottom.equalTo()(square.mas_bottom)
            make?.left.equalTo()(squareCell.mas_left)?.offset()(-0.5)
            make?.width.equalTo()(1)
        }
        
    }
    
    func buildHorizontalLine(_ squareCell: SquareCell) {
        let line = UIView(frame: .zero)
        line.backgroundColor = .black
        addSubview(line)
        
        line.mas_makeConstraints{ (make) in
            make?.left.equalTo()(square.mas_left)
            make?.right.equalTo()(square.mas_right)
            make?.top.equalTo()(squareCell.mas_top)?.offset()(-0.5)
            make?.height.equalTo()(1)
        }
        
    }
    
    func squareAt(y: Int, x: Int) -> SquareCell? {
        
        let idx = y * Int(Double(numSquares).squareRoot()) + x
        
        guard idx < squares.count else {
            return nil
        }
        
        return squares[idx]
    }
    
    func setNumbers(_ numbers: [Int]) {
        
        for i in 0..<min(squares.count, numbers.count) {
            
            squares[i].number = numbers[i]
            solutionNumbers[i] = numbers[i]
        }
    }
    
    func setEnteredNumbers(_ numbers: [Int]) {
        
        for i in 0..<min(squares.count, numbers.count) {
            
            squares[i].enteredNumber = numbers[i]
            enteredNumbers[i] = numbers[i]
            
//            if squares[i].enteredNumber != 0 {
//                if squares[i].number != squares[i].enteredNumber {
//                    squares[i].labelTextColor = .red
//                } else {
//                    squares[i].labelTextColor = .blue
//                }
//            }
        }
    }
    
    func hideNumbersByIndex(_ indexArray: [Int]) {
        
        for i in 0..<indexArray.count {
            squares[indexArray[i]].hideLabelNumber()
            squares[indexArray[i]].isEditable = true
        }
    }
    
    func resetEditableStatus() {
        squares.forEach{
            $0.isEditable = false
            $0.enteredNumber = 0
//            $0.resetColor()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: self)
           
            let calcIndex = Int(Int(position.x) / 40) + Int(Int(position.y) / 40) * 9
            //squares[calcIndex].changeColor(newColor: .green)
            
            indexXY = (calcIndex % 9 , calcIndex / 9)
        
            print("\(indexXY.0), \(indexXY.1)")
            print("\(calcIndex)")
            
            clearColorSquares()
            highlightedSquares(index: indexXY)
            selectedSquare = squareAt(y: indexXY.1, x: indexXY.0)
            
            print(selectedSquare?.number)
            print(selectedSquare?.enteredNumber)
            print(selectedSquare?.labelTextColor)
            
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: self)
            let calcIndex = Int(Int(position.x) / 40) + Int(Int(position.y) / 40) * 9
            //squares[calcIndex].hideLabelNumber()
            
            indexXY = (calcIndex % 9 , calcIndex / 9)
            
        }
    }
    
    func gameWon() -> Bool{
        let cells = editableCells()
        var correctLabels = 0
        for i in 0..<cells.count {
            
            if (cells[i].isLocked() &&
                !cells[i].corectTextLabel()) ||
                (!cells[i].isLocked()){
                break
            }
    
            correctLabels = correctLabels + 1
        }
        
        return ( correctLabels == cells.count )
        
    }
    
    func initialHighlightedSquares() {
        let index = indexXY
        let XY = Int(Double(numSquares).squareRoot())
        //highlight squares on vertical and horizontal
        for z in 0..<XY {
            squareAt(y: z, x: index.0)?.changeColor(newColor: .systemGray5)
            squareAt(y: index.1, x: z)?.changeColor(newColor: .systemGray5)
            
        }
        
        //highlight squares that are in the same group
        let id = squareAt(y: index.1, x: index.0)?.groupId
        let sameGroupIdSquares = sameGroupId(groupId: id!)
        for i in 0..<sameGroupIdSquares.count {
            
            sameGroupIdSquares[i].changeColor(newColor: .systemGray5)
        }
        //highlight active selected square
        let myColour = UIColor(red: 168/255, green: 192/255, blue: 219/255, alpha: 1.0)
        squareAt(y: index.1, x: index.0)?.changeColor(newColor: myColour)
    }
    
    func highlightedSquares(index: (Int, Int)) {
        let XY = Int(Double(numSquares).squareRoot())
        //highlight squares on vertical and horizontal
        for z in 0..<XY {
            squareAt(y: z, x: index.0)?.changeColor(newColor: .systemGray5)
            squareAt(y: index.1, x: z)?.changeColor(newColor: .systemGray5)
            
        }
        
        //highlight squares that are having the same visible number
        if (squareAt(y: index.1, x: index.0)?.isLocked())!  {
            var labelNumber = squareAt(y: index.1, x: index.0)!.enteredNumber
            if labelNumber == 0 {
                labelNumber = squareAt(y: index.1, x: index.0)!.number
            }
            let sameLabelNumberSquares = sameLabelNumber(labelNumber: labelNumber)
            for i in 0..<sameLabelNumberSquares.count {
                if sameLabelNumberSquares[i].isLocked() && sameLabelNumberSquares[i].corectTextLabel(){
                    sameLabelNumberSquares[i].changeColor(newColor: .systemGray3)
                }
            }
        }
        
        //highlight squares that are in the same group
        let id = squareAt(y: index.1, x: index.0)?.groupId
        let sameGroupIdSquares = sameGroupId(groupId: id!)
        for i in 0..<sameGroupIdSquares.count {
            
            sameGroupIdSquares[i].changeColor(newColor: .systemGray5)
        }
        //highlight active selected square
        let myColour = UIColor(red: 168/255, green: 192/255, blue: 219/255, alpha: 1.0)
        squareAt(y: index.1, x: index.0)?.changeColor(newColor: myColour)
    }
    
    func clearColorSquares() {
        for i in 0..<squares.count {
            squares[i].resetColor()
        }
    }
    
    func sameLabelNumber(labelNumber: Int) -> [SquareCell] {
        var tmpSquares: [SquareCell] = []
        for i in 0..<squares.count {
            if true == squares[i].hasSquareNumber(number: labelNumber) {
                tmpSquares.append(squares[i])
            }
        }
        return tmpSquares
    }
    
    func sameGroupId(groupId: Int) -> [SquareCell] {
        var tmpSquares: [SquareCell] = []
        for i in 0..<squares.count {
            if groupId == squares[i].groupId {
                tmpSquares.append(squares[i])
            }
        }
        return tmpSquares
    }
    
    func editableCells() -> [SquareCell] {
        var tmpSquares: [SquareCell] = []
        for i in 0..<squares.count {
            if squares[i].isEditable {
                tmpSquares.append(squares[i])
            }
        }
        return tmpSquares
    }
    
    func resetGame() {
        mistakesCounter = 0
        
    }
    
    func countInSquares(for number: Int) -> Int {
        var count = 0
        
        let sameLabelNumberSquares = sameLabelNumber(labelNumber: number)
        for i in 0..<sameLabelNumberSquares.count {
            if sameLabelNumberSquares[i].isLocked() && sameLabelNumberSquares[i].corectTextLabel(){
                count = count + 1
            }
        }
        
        return count
    }
    
    func eraseLabel(for cell: SquareCell) {
        if cell.corectTextLabel() &&
            countInSquares(for: cell.number) == 9 {
            self.delegate?.unhideNumberSelection(with: cell.number)
        }
        //cell.index
        enteredNumbers[cell.index] = 0
        cell.eraseLabelNumber()
        
    }
}

class Sudoku: Square {
    
    fileprivate override var numSquares: Int {
        return 81
    }
}

extension Square: NumberSelectionDelegate {
    
    func didSelect(number: Int) {
        //isLocked()
        if (selectedSquare!.isEditable) {
            selectedSquare!.enteredNumber = number
            enteredNumbers[selectedSquare!.index] = number
            if selectedSquare!.number != number {
                if countInSquares(for: selectedSquare!.number) != 9 {
                    self.delegate?.unhideNumberSelection(with: selectedSquare!.number)
                }
                mistakesCounter = mistakesCounter + 1
                self.delegate?.updateMistakeCounter(value: mistakesCounter)
                if mistakesCounter == 3 {
                    print("maxim number of mistakes")
                    
                    self.delegate?.presentAlert()
                    resetGame()
                }
            } else {
                
                if gameWon() {
                    print("game is done")
                    self.delegate?.presentGameFinishedNotification()
                }
                
                print("\(number) is available in \(countInSquares(for: number)) squares")
                if countInSquares(for: number) == 9 {
                    self.delegate?.hideNumberSelection(with: number)
                }
            }
        }

    }
}
