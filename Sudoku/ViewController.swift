//
//  ViewController.swift
//  Sudoku
//
//  Created by Camil Milos on 23/11/2019.
//  Copyright © 2019 Camil Milos. All rights reserved.
//

import GoogleMobileAds
import UIKit
import RealmSwift


class ViewController: UIViewController {
    @IBOutlet weak var basicView: UIView!
    @IBOutlet weak var numberSelectionView: UIView!
    @IBOutlet weak var commercialView: UIView!
    
    @IBOutlet weak var mistakesCounter: UILabel!
    
    @IBAction func eraseLabelCell(_ sender: Any) {
       // sudokuSquare.selectedSquare?.eraseLabelNumber()
        sudokuSquare.eraseLabel(for: sudokuSquare.selectedSquare!)
    }
    
    var sudokuSquare: Sudoku!
    var numberSelection: NumberSelectionView!
    
    var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupSudokuView()
        setupSelectionNumbersView()
        
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.delegate = self
        addBannerViewToView(bannerView)
        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        //[ "00008030-000114891A78802E" ] //my phone ID: 00008030-000114891A78802E
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "624aa7b2a007f4fd47c10da34e09ef3b" ]
        bannerView.load(GADRequest())
        
        let realm = try! Realm()
        print(Realm.Configuration.defaultConfiguration.fileURL)

        let results = realm.objects(StorageEntity.self)
        print(results.count)
             //   let localStorage = realm.objects(StorageEntity.self).first!

//            let newStorage = StorageEntity()
//
//            try! realm.write {
//                realm.add(newStorage)
//                newStorage.name = "TEST13"
//
//                for item in sudokuSquare.solutionNumbers {
//                    newStorage.appendSolutionNumber(solutionNumber: item)
//                }
//
//                for item in //sudokuSquare.enteredNumbers
//                 [0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
//                         7,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]{
//                    newStorage.appendEnteredNumber(enteredNumber: item)
//                } 
//            }
//
//        let results = realm.objects(StorageEntity.self).filter("name = 'TEST13'")
//        print(results[0].enteredNumbers)
//
//        var enteredNumbers2: [Int] = [0]
//        enteredNumbers2.removeAll()
//
//        for number in results[0].enteredNumbers {
//            enteredNumbers2.append(number)
//        }
//
//        print(enteredNumbers2[4])
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.save), name: UIApplication.willResignActiveNotification, object: UIApplication.shared)
        
    }
    
    @objc func save() {
        print("end")
        let newStorage = StorageEntity()
        newStorage.storageUniqueID = 1
        let realm = try! Realm()
        try! realm.write {
            realm.add(newStorage, update: .all)

            for item in sudokuSquare.solutionNumbers {
                newStorage.appendSolutionNumber(solutionNumber: item)
            }

            for item in sudokuSquare.enteredNumbers {
                newStorage.appendEnteredNumber(enteredNumber: item)
            }
        }
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
     bannerView.translatesAutoresizingMaskIntoConstraints = false
     view.addSubview(bannerView)
     view.addConstraints(
       [NSLayoutConstraint(item: bannerView,
                           attribute: .bottom,
                           relatedBy: .equal,
                           toItem: view.safeAreaLayoutGuide, //bottomLayoutGuide,
                           attribute: .bottom,// .top,
                           multiplier: 1,
                           constant: -10),
        NSLayoutConstraint(item: bannerView,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: view,
                           attribute: .centerX,
                           multiplier: 1,
                           constant: 0)
       ])
    }
    
    fileprivate func setupSudokuView() {
        sudokuSquare = Sudoku()
        sudokuSquare.delegate = self
        //sudokuSquare.delegateNumberSelection = self.numberSelection
    
        let realm = try! Realm()
        print(Realm.Configuration.defaultConfiguration.fileURL)

        if realm.objects(StorageEntity.self).count != 0 {
            
            let results = realm.objects(StorageEntity.self)
            print(results[0].enteredNumbers)
            
            var solutionNumbers2: [Int] = [0]
            solutionNumbers2.removeAll()
    
            for number in results[0].solutionNumbers {
                solutionNumbers2.append(number)
            }
            
            sudokuSquare.setNumbers(solutionNumbers2)
            
           // sudokuSquare.hideNumbersByIndex([1,3,4,56,59,60])
            sudokuSquare.hideNumbersByIndex([1, 3, 4, 17, 19, 21, 22, 28, 30, 32, 33, 34, 35, 36, 38, 40, 41, 43,
            45, 47, 68, 70, 71, 72, 74, 80])
    
            var enteredNumbers2: [Int] = [0]
            enteredNumbers2.removeAll()
    
            for number in results[0].enteredNumbers {
                enteredNumbers2.append(number)
            }
            
            sudokuSquare.setEnteredNumbers(enteredNumbers2)
            
            
            
            
        }
        else {
            sudokuSquare.setNumbers([9,2,7,4,6,3,5,8,1,1,4,8,7,5,9,6,3,2,3,5,6,8,2,1,4,9,7,7,8,4,1,3,5,2,6,9,2,3,9,6,8,7,1,4,5,5,6,1,2,9,4,8,7,3,8,9,
            5,3,4,2,7,1,6,4,7,3,5,1,6,9,2,8,6,1,2,9,7,8,3,5,4])
            sudokuSquare.hideNumbersByIndex([1,3,4,56,59,60])
            
        }
        
        
//        sudokuSquare.setNumbers([9,2,7,4,6,3,5,8,1,1,4,8,7,5,9,6,3,2,3,5,6,8,2,1,4,9,7,7,8,4,1,3,5,2,6,9,2,3,9,6,8,7,1,4,5,5,6,1,2,9,4,8,7,3,8,9,
//        5,3,4,2,7,1,6,4,7,3,5,1,6,9,2,8,6,1,2,9,7,8,3,5,4])
        
        
        
//        sudokuSquare.hideNumbersByIndex([1, 3, 4, 6, 7, 11, 14, 15, 17, 19, 21, 22, 28, 30, 32, 33, 34, 35, 36, 38, 40, 41, 43,
//        45, 47, 48, 49, 55, 56, 59, 60, 64, 68, 70, 71, 72, 74, 80])
//        sudokuSquare.hideNumbersByIndex([1,3,4,56,59,60])
        
        
//
//        sudokuSquare.setEnteredNumbers([0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
//        7,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        
        basicView.layer.borderColor = UIColor.black.cgColor;
        basicView.layer.borderWidth = 1.5
        basicView.addSubview(sudokuSquare)
        sudokuSquare.mas_makeConstraints { (make) in
            
            make?.edges.equalTo()(basicView)
        }
    }
    
    fileprivate func setupSelectionNumbersView() {
        numberSelection = NumberSelectionView()
        numberSelection.delegate = self.sudokuSquare
        numberSelectionView.addSubview(numberSelection)
        numberSelection.mas_makeConstraints{ (make) in
            make?.edges.equalTo()(numberSelectionView)
        }
        
    }
    
    func showGameDoneNotification() {
        
        // create the alert
        let alert = UIAlertController(title: "Game Over", message: "You have made 3 mistake and lost this game", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "New Game", style: UIAlertAction.Style.default, handler: { action in
            self.resetGame()
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func showGameFinishedNotification() {
        
        // create the alert
        let alert = UIAlertController(title: "Won", message: "You are an excelent player!", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "New Challenge?", style: UIAlertAction.Style.default, handler: { action in
            self.newGame()
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func resetGame() {
        
        let editableCells = sudokuSquare.editableCells()
        for i in 0..<editableCells.count {
            editableCells[i].eraseLabelNumber()
        }
        mistakesCounter.text = "0"
        numberSelection.resetNumbers()
    }
    
    fileprivate func newGame() {
        print("New game must be generated now!")

        //basicView.willRemoveSubview(sudokuSquare)
        sudokuSquare.removeFromSuperview()
        sudokuSquare = nil
        sudokuSquare = Sudoku()
        sudokuSquare.delegate = self
        
        //step 1 - choose an unique solution from solution generator array
//        sudokuSquare.resetEditableStatus()
        sudokuSquare.setNumbers([4, 8, 5, 7, 1, 9, 2, 6, 3, 3, 7, 6, 4, 2, 5, 8, 9, 1, 1, 2, 9,3,8,6,4,7,5,2,3,8,5,
        4,7,9,1,6,9,5,4,1,6,8,3,2,7,6,1,7,9,3,2,5,8,4,8,4,3,6,9,1,7,5,2,7,6,2,8,5,3,1,4,9,5,9,1,2,7,4,6,3,8])
        sudokuSquare.hideNumbersByIndex([1, 3, 4, 17, 19, 21, 22, 28, 30, 32, 33, 34, 35, 36, 38, 40, 41, 43,
        45, 47, 68, 70, 71, 72, 74, 80])
        sudokuSquare.setEnteredNumbers([])
        
        
        
//        numberSelection.resetNumbers()
        //step 2 - hide randomly labels
        basicView.addSubview(sudokuSquare)
        sudokuSquare.mas_makeConstraints { (make) in
            
            make?.edges.equalTo()(basicView)
        }
        numberSelection.removeFromSuperview()
        //numberSelectionView.willRemoveSubview(numberSelection)
        numberSelection = nil
        numberSelection = NumberSelectionView()
        numberSelection.delegate = self.sudokuSquare
        numberSelectionView.addSubview(numberSelection)
        numberSelection.mas_makeConstraints{ (make) in
            make?.edges.equalTo()(numberSelectionView)
        }
        
        mistakesCounter.text = "0"
    }

}

extension ViewController: SquareDelegate {
    
    func hideNumberSelection(with index: Int) {
        numberSelection.hideNumberSelection(with: index)
    }
    
    func unhideNumberSelection(with index: Int) {
        numberSelection.unhideNumberSelection(with: index)
    }
    
    func updateMistakeCounter(value: Int) {
        mistakesCounter.text = "\(value)"
    }
    
    func presentAlert() {
        showGameDoneNotification()
    }
    
    func presentGameFinishedNotification() {
        showGameFinishedNotification()
    }
    
}

extension ViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("adViewDidReceiveAd")
    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: GADRequestError) {
      print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }
}

