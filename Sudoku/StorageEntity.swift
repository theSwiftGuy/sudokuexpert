//
//  StorageEntity.swift
//  Sudoku
//
//  Created by Camil Milos on 24/12/2019.
//  Copyright © 2019 Camil Milos. All rights reserved.
//

import Foundation
import RealmSwift

//class StorageEntity: Object {
//    @objc dynamic var solutionNumbers: [Int] = [0]
//    @objc dynamic var enteredNumbers: [Int] = [0]
//}

class StorageEntity: Object {
    @objc dynamic var storageUniqueID = 1
    override static func primaryKey() -> String? {
      return "storageUniqueID"
    }
    let solutionNumbers = List<Int>()
    let enteredNumbers = List<Int>()
    //@objc dynamic var name: String = ""
    
    func appendSolutionNumber(solutionNumber: Int) {
     // guard let _ = realm else { fatalError("Can't use this one detached object") }

      solutionNumbers.append(solutionNumber)

    }
    
    func appendEnteredNumber(enteredNumber: Int) {
      //guard let _ = realm else { fatalError("Can't use this one detached object") }

      enteredNumbers.append(enteredNumber)

    }
}
